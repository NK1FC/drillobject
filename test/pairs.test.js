const pairs = require('../pairs.cjs');


const a={ one: 1, two: 2, three: 3 };

test('testing pairs', () => {
    expect(pairs(a)).toStrictEqual([["one", 1], ["two", 2], ["three", 3]])
})