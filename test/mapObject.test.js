const mapObject = require("../mapObject.cjs");


function cb(element){
    return 2*element;
}

let a={b:2, c:3, d:4};


test('Map Objects', ()=>{
    expect(mapObject(a,cb)).toStrictEqual({b: 4, c: 6, d: 8})
})

