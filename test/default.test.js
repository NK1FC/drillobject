const defaults = require('../default.cjs');

let iceCream = { flavor: "chocolate" };
let defaultObj = 'sas';

console.log(defaults(iceCream, defaultObj));

test('Defaults', () => {
    expect(defaults(iceCream, defaultObj))
        .toStrictEqual({ 0: 's', 1: 'a', 2: 's', flavor: 'chocolate' });
})


