function defaults(obj, defaultObj){
    try{
        let result={...obj};
        for(let key in defaultObj){
            if(result.hasOwnProperty(key)===false){
                result[key]=defaultObj[key];
            }
        }
        return result;
    }catch(err){
        console.log(err);
        return obj
    }
    return obj;
}



module.exports=defaults;