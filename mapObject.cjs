function mapObject(obj, cb){
    try{
        let result={};
        for(let key in obj){
            result[key]=cb(obj[key],key);
        }
        return result;
    }
    catch(err){
        return {};
    }
    return {};
}


module.exports=mapObject;